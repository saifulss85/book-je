import axios from 'axios/index';
import React, { Component } from 'react';
import { AppHeader } from '../components/AppHeader/AppHeader';
import { AXIOS_CONFIG, BASE_URL } from '../constants/ApiConstants';

export class BookView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      book: undefined
    };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    const bookId = this.props.match.params.bookId;

    axios.get(BASE_URL + '/books/' + bookId, AXIOS_CONFIG)
      .then((response) => {
        this.setState({
          book: {
            id: response.data._id.$oid,
            title: response.data.title,
            sellerId: response.data.seller_id,
          }
        });
      });
  }

  onSubmit(event) {
    event.preventDefault();

    console.log(event);
  }

  render() {
    if (this.state.book === undefined) return <div>loading...</div>;

    return (
      <div className="App">
        <AppHeader/>

        <div>
          <span>Title: </span><span>{this.state.book.title}</span>
          <br/>
          <span>Seller: </span><span>{this.state.book.sellerId}</span>
          <br/>
          <span>Status: </span><span>Available | Sold</span>
        </div>

        <form style={{marginTop: '28px'}} onSubmit={(event) => this.onSubmit(event)}>
          <input type="text" placeholder="Amount"/>
          <input type="submit" value="Make Offer"/>
        </form>

      </div>
    );
  }
}
