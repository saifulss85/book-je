import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import '../components/App/App.css';
import { AppHeader } from '../components/AppHeader/AppHeader';

export class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldNavigateToLogin: false
    };

    this.onSignInClick = this.onSignInClick.bind(this);
  }

  render() {
    if (this.state.shouldNavigateToLogin) return <Redirect push to="login"/>;

    return (
      <div className="App">
        <AppHeader/>

        <p className="App-intro">
          Your one-stop portal for grabbing used books
        </p>

        <input type="button" value="Sign in" onClick={this.onSignInClick}/>

      </div>
    );
  }

  onSignInClick() {
    this.setState({
      shouldNavigateToLogin: true
    });
  }
}