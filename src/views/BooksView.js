import axios from 'axios';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { AppHeader } from '../components/AppHeader/AppHeader';
import { Book } from '../components/Book/Book';
import { AXIOS_CONFIG, BASE_URL } from '../constants/ApiConstants';

export class BooksView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasFetchedBooks: false,
      books: [],
      searchTerm: ''
    };

    this.onSearchTermEntered = this.onSearchTermEntered.bind(this);
    this.fetchBooks = this.fetchBooks.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.fetchBooks();
  }

  fetchBooks() {
    let config = AXIOS_CONFIG;
    config.params = {
      filter: {
        'title': {
          '$regex': '(?i)' + this.state.searchTerm
        }
      }
    };

    axios.get(BASE_URL + '/books', config)
      .then((response) => {
        let books = response.data._embedded.map((bookResponse) => {
          const book = {
            id: bookResponse._id.$oid,
            title: bookResponse.title,
            sellerId: bookResponse.seller_id,
          };

          return <Book key={book.id} book={book}/>;
        });

        if (books.length === 0) books = <div>No hits</div>;

        this.setState({
          hasFetchedBooks: true,
          books,
          searchTerm: ''
        });

      });

  }

  onSearchTermEntered(event) {
    this.setState({
      searchTerm: event.target.value
    });
  }

  onSubmit(event) {
    event.preventDefault();
    this.fetchBooks();
  }

  render() {
    if (!this.state.hasFetchedBooks) return <div>loading...</div>;

    return (
      <div className="App">
        <AppHeader/>

        {this.props.session.userEmail &&
        <h4>Welcome, {this.props.session.userEmail}</h4>
        }

        <h6>These are the available books. Click on a book and maybe something will happen. I think. Maybe.</h6>

        <form onSubmit={(event) => this.onSubmit(event)}>
          <input autoFocus type="text" placeholder="Search" value={this.state.searchTerm} onChange={(event) => this.onSearchTermEntered(event)}/>
          <input type="button" value="Go" onClick={this.fetchBooks} style={{ marginBottom: '28px' }}/>
        </form>

        <div style={{ textAlign: 'center' }}>
          {this.state.books}
        </div>

      </div>
    );
  }
}

BooksView.propTypes = {
  session: PropTypes.object,
};