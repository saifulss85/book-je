import axios from 'axios/index';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { AppHeader } from '../components/AppHeader/AppHeader';
import { AXIOS_CONFIG, BASE_URL } from '../constants/ApiConstants';

export class CreateBookView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      bookTitle: ''
    };

    this.onTitleChange = this.onTitleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onTitleChange(event) {
    this.setState({
      bookTitle: event.target.value,
      shouldNavigateToBooksView: false
    });
  }

  onSubmit(event) {
    event.preventDefault();

    if (this.props.session.userEmail === '' || this.props.session.userEmail === undefined) throw new Error('user email is empty');
    if (this.state.bookTitle === '' || this.state.bookTitle === undefined) throw new Error('enter a title lah dey');

    const data = {
      "seller_id": this.props.session.userEmail,
      "title": this.state.bookTitle
    };

    axios.post(BASE_URL + '/books', data, AXIOS_CONFIG)
      .then((response) => {
        console.log(response);

        this.setState({
          shouldNavigateToBooksView: true
        })
      });
  }

  render() {
    if (this.state.shouldNavigateToBooksView) return <Redirect to="/books"/>;

    return (
      <div className="App">
        <AppHeader shouldShowAddBookLink={false}/>

        <form style={{ marginTop: '28px' }} onSubmit={(event) => this.onSubmit(event)}>
          <input
            type="text"
            placeholder="Title"
            value={this.state.bookTitle}
            onChange={(event) => this.onTitleChange(event)}
          />
          <input type="submit" value="Add"/>
        </form>

      </div>
    );
  }
}

CreateBookView.propTypes = {
  session: PropTypes.object
};
