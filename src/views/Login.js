import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { AppHeader } from '../components/AppHeader/AppHeader';

export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldNavigateToBooks: false,
      userEmail: ''
    };

    this.onSignInClick = this.onSignInClick.bind(this);
  }

  render() {
    if (this.state.shouldNavigateToBooks) return <Redirect push to="books"/>;

    return (
      <div className="App">
        <AppHeader shouldShowAddBookLink={false}/>

        <div style={{ marginTop: '32px' }}>
          <h3>Book In</h3>
          <input
            type="text"
            placeholder="Email"
            value={this.state.userEmail}
            onChange={(event) => this.setState({ userEmail: event.target.value })}
            style={{ marginBottom: '8px' }}
          />
          <br/>
          <input type="button" value="Go" onClick={this.onSignInClick}/>
        </div>

      </div>
    );
  }

  onSignInClick() {
    this.props.onUserSignedIn(this.state.userEmail);

    this.setState({
      shouldNavigateToBooks: true
    });
  }
}

Login.propTypes = {
  onUserSignedIn: PropTypes.func
};