import PropTypes from 'prop-types';
import React, { Component } from "react";
import { Link } from 'react-router-dom';
import logo from '../../assets/book.png';
import './AppHeader.css';

export class AppHeader extends Component {
  render() {
    return (
      <header className="AppHeader">

        <Link to="/books" className="AppHeader--link">
          <img src={logo} className="AppHeader--logo" alt="logo"/>
          <h1 className="AppHeader--title">Book Je!</h1>
        </Link>

        {this.props.shouldShowAddBookLink &&
        <Link to="/books/create" className="AppHeader--link AppHeader--add-book-link">
          Add Book
        </Link>
        }

      </header>
    );
  }
}

AppHeader.propTypes = {
  shouldShowAddBookLink: PropTypes.bool
};

AppHeader.defaultProps = {
  shouldShowAddBookLink: true
};