import PropTypes from 'prop-types';
import React, { Component } from "react";
import { Redirect } from 'react-router-dom';
import './Book.css';

export class Book extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shouldNavigateToBook: false
    };

    this.onBookClick = this.onBookClick.bind(this);
  }

  render() {
    if (this.state.shouldNavigateToBook) return <Redirect push to={'books/' + this.props.book.id}/>;

    return (
      <div onClick={this.onBookClick} className="Book">
        <div className="Book--title">{this.props.book.title}</div>
        <div className="Book--seller">{this.props.book.sellerId}</div>
      </div>
    );
  }

  onBookClick() {
    this.setState({
      shouldNavigateToBook: true
    });
  }
}

Book.propTypes = {
  book: PropTypes.object
};