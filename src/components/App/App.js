import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Switch from 'react-router-dom/es/Switch';
import { BookView } from '../../views/BookView';
import { BooksView } from '../../views/BooksView';
import { CreateBookView } from '../../views/CreateBookView';
import { Error404 } from '../../views/Error404';
import { Home } from '../../views/Home';
import { Login } from '../../views/Login';
import './App.css';

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      session: {
        userEmail: undefined
      }
    };

    this.onUserSignedIn = this.onUserSignedIn.bind(this);
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/login" render={() => <Login onUserSignedIn={this.onUserSignedIn}/>}/>
            <Route exact path="/books" render={() => <BooksView session={this.state.session}/>}/>
            <Route path="/books/create" render={() => <CreateBookView session={this.state.session}/>}/>
            <Route path="/books/:bookId" component={BookView}/>
            <Route component={Error404}/>
          </Switch>
        </div>
      </BrowserRouter>
    );
  }

  onUserSignedIn(userEmail) {
    this.setState({
      session: {
        userEmail
      }
    });
  }
}
